<?php
session_start();

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Future Value Calculator</title>
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<body>
  <div class="container">
    <?php if (isset($_SESSION["errors-list"])) {
      echo "<pre>".$_SESSION["errors-list"]."</pre>";

    }
      ?>
    <form action="./submit.php" method="POST">
      <div class="form-group row">
        <label class="col-sm-3 col-form-label"for="invmAmount">Investment Amount</label>
        <input type="text" class="form-control col-sm-9" name="invmAmount">
      </div>
      <div class="form-group row">
        <label class="col-sm-3 col-form-label"for="yrItrRate">Yearly Interest Rate</label>
        <input type="text" class="form-control col-sm-9" name="yrItrRate">
      </div>

      <div class="form-group row">
        <label class="col-sm-3 col-form-label"for="numYear">Number of Year</label>
        <input type="text" class="form-control col-sm-9" name="numYear">
      </div>
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
  </div>
</body>
<style>
  .container {
    width: 1000px;
    margin: 100px auto;
  }
</style>

</html>