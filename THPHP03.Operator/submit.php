<?php
session_start();
$errmsg = "";
function checkValid() {
  $con = true;
  global $errmsg;
  if (!(isset($_POST["invmAmount"]) && is_numeric($_POST["invmAmount"]))) {
    $errmsg = $errmsg."Investment must be a valid number\n";
    $con = false;
  }
  if (!(isset($_POST["yrItrRate"]) && is_numeric($_POST["yrItrRate"]))) {
    $errmsg = $errmsg."Yearly interest rate must be a valid number\n";
    $con = false;
  }
  if (!(isset($_POST["numYear"]) && is_numeric($_POST["numYear"]))){
    $errmsg = $errmsg."Number of years must be a valid number\n";
    $con = false;
  }
  return $con;
}

if (!checkValid()) {
  $_SESSION["errors-list"] = $errmsg;
  header("Location: ./index.php");
}
else {
  unset($_SESSION["errors-list"]);
}
$invmAmount = $_POST["invmAmount"];
$yrItrRate = $_POST["yrItrRate"];
$numYear = $_POST["numYear"];
$futureValue=$invmAmount*pow((1+$yrItrRate/100),$numYear);
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Future Value Calculator</title>
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<body>
  <div class="container">
    <form action="./submit.php" method="POST">
      <div class="form-group row">
        <label class="col-sm-3 col-form-label"for="invmAmount">Investment Amount</label>
        <p class="form-control col-sm-9"><?php echo "$".number_format($invmAmount,2,".",",")?></p>
      </div>
      <div class="form-group row">
        <label class="col-sm-3 col-form-label"for="yrItrRate">Yearly Interest Rate</label>
        <p class="form-control col-sm-9" ><?php echo number_format($yrItrRate,2,".",",")."%"?></p>
      </div>

      <div class="form-group row">
        <label class="col-sm-3 col-form-label"for="numYear">Number of Year</label>
        <p class="form-control col-sm-9" ><?php echo $numYear?></p>
      </div>
      <div class="form-group row">
        <label class="col-sm-3 col-form-label"for="numYear">Future value</label>
        <p class="form-control col-sm-9" ><?php echo "$".number_format($futureValue,2,".",",")?></p>
      </div>
    </form>
  </div>
</body>
<style>
  .container {
    width: 1000px;
    margin: 100px auto;
  }
</style>

</html>