<?php
$result = "";
if (isset($_GET["number"])) {
  $number = $_GET["number"];

  switch ($number) {
    case 1:
      $result = "một";
      break;
    case 2:
      $result = "hai";
      break;
    case 3:
      $result = "ba";
      break;
    case 4:
      $result = "bốn";
      break;
    case 5:
      $result = "năm";
      break;
    case 6:
      $result = "sáu";
      break;
    case 7:
      $result = "bảy";
      break;
    case 8:
      $result = "chín";
      break;
    case 9:
      $result = "chín";
      break;
    default:
      $result = "bạn phải nhập đúng số";
  }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <?php
  require_once("./modules/jquery_boostrap_fontawesome.php");
  ?>
</head>

<body>
  <div class="container">
    <form action="index.php">
      <div class="form-group">
        <label for="number">Chuyển đổi số thành chữ</label>
        <input type="text" class="form-control" name="number">
      </div>
      <button type="submit" class="btn btn-primary">Chuyển đổi</button>
    </form>

    <div class="form-group" id="result">
      <label for="number">Kết quả</label>
      <input type="text" class="form-control" readonly value="<?php echo $result?>">
    </div>
  </div>
  </div>
</body>
<style>
  #result {
    margin: 10px auto;
  }
</style>

</html>