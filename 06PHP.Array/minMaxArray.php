<?php
$n = "";
$arr = array(0);
if (isset($_GET["n"])) {
  $n = $_GET["n"];
  if (!!$n) {
    $arr = array($n);
    for ($i=0; $i<$n; $i++) {
      $arr[$i] = mt_rand(0,1000);
    }
  }
}

function returnArrayString() {
  global $arr;
  return (implode(",",$arr));
}

function findMax() {
  global $arr;
  $max = null;
  for ($i=0; $i<count($arr); $i++) {
    $max = (!!$max)?($max>=$arr[$i]?$max:$arr[$i]): $arr[$i];
  }
  return $max;
}

function findMin() {
  global $arr;
  $min = null;
  for ($i=0; $i<count($arr); $i++) {
    $min = (!!$min)?($min<=$arr[$i]?$min:$arr[$i]): $arr[$i];
  }
  return $min;
}

function sumArray() {
  global $arr;
  $sum = 0;
  for ($i=0; $i<count($arr); $i++) {
    $sum+=$arr[$i];
  }
  return $sum;
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>

<body>
  <div class="container">
    <div class="box">
      <div class="box-header">
        <h1>Phát sinh mảng và tính toán</h1>
      </div>
      <form action="" id="form">
      <div class="box-body">

        <label for="n">Nhập số phần tử</label>
        <input type="text" name="n" placeholder="<?php echo $n ?>">
        <button class="span" >Phát sinh và tính toán</button>
      </div>
      </form>
      <div class="box-body">
        <label for="array">Mảng</label>
        <input type="text" value="<?php echo returnArrayString();?>" disabled>
        <label for="max">MAX</label>
        <input type="text" value="<?php echo findMax()?>" disabled>
        <label for="max">MIN</label>
        <input type="text" value="<?php echo findMin()?>" disabled>
        <label for="max">Tổng mảng</label>
        <input type="text" value="<?php echo sumArray();?>">
        
      </div>
    </div>
  </div>
</body>
<style>
  * {
    margin: 0;
    padding: 0;
  }
  .container {
    width: 1000px;
    margin: 100px auto;

  }
  body {
    font-family: sans-serif;
  }
  .box {
    box-sizing: border-box;
    border: 1px solid lightblue;
    padding: 20px;
    border-radius: 3px;
    box-shadow: 0 0 1px 2px lightskyblue;
  }
  .box-header {
    text-align: center;
    font-size: 1.1em;
    font-weight: 100;
    margin-bottom: 10px;
  }
.span {
  grid-column-start: 1;
  grid-column-end: 3;
}
  .box-body {
    margin: 10px 0;
    display:  grid;
    grid-template-columns: 1fr 1fr;
    grid-auto-rows: 30px;
    grid-gap: 10px;
    justify-items: stretch;
  }
  input[type="text"] {
    padding: 5px;
  }
</style>
</html>