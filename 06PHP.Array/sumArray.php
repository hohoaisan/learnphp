<?php

$placeholder  = "";
function sum() {
  global $placeholder;
  $sum = 0;
  if (isset($_GET["listnumber"])) {
    $placeholder = $_GET["listnumber"];
    if (!!$_GET["listnumber"]) {
      $processString = trim($_GET["listnumber"], " ,");
      $arr = explode(",", $processString);
      try {
        for ($i = 0; $i < count($arr); $i++) {
          if (is_numeric($arr[$i])) {
            $sum += $arr[$i];
          } else {
            throw new Exception("Dãy số được nhập vào phải hợp lệ và ngăn cách bởi dấu phẩy");
          }
        }
      } catch (Exception $e) {
        die($e->getMessage());
      }
    }
  }
  return $sum;
}


?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <?php
  require_once("./modules/jquery_boostrap_fontawesome.php");
  ?>
</head>

<body>
  <div class="container">
    <form action="">
      <div class="form-group">
        <label for="number">Nhập và tính trên dãy số</label>
        <input type="text" class="form-control" name="listnumber" placeholder="<?php echo $placeholder ?>">
      </div>
      <button type="submit" class="btn btn-primary">Tính tổng</button>
    </form>

    <div class="form-group" id="result">
      <label for="">Tổng dãy số</label>
      <input type="text" class="form-control" readonly value="<?php echo sum(); ?>">
    </div>
  </div>
  </div>
</body>
<style>
  #result {
    margin: 10px auto;
  }
</style>

</html>