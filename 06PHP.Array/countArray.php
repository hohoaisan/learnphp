<?php

$placeholder  = "";
$arr = null;
if (isset($_GET["listnumber"])) {
    $placeholder = $_GET["listnumber"];
    if (!!$_GET["listnumber"]) {
      $processString = trim($_GET["listnumber"], " ,");
      $arr = explode(",", $processString);
      try {
        for ($i = 0; $i < count($arr); $i++) {
          if (!is_numeric($arr[$i])) {
            throw new Exception("Dãy số được nhập vào phải hợp lệ và ngăn cách bởi dấu phẩy");
          }
        }
      } catch (Exception $e) {
        die($e->getMessage());
      }
    }
  }
  
  function countDuplicate($arr) {
    $tmp = array();
    for ($i=0; $i<count($arr); $i++) {
      if (array_key_exists($arr[$i],$tmp)) {
        $tmp[$arr[$i]]++;
      }
      else $tmp[$arr[$i]] = 1;
    }
    return $tmp;
  }
  
  

  function assocArrayToString($arr) {
    $reducedArr = countDuplicate($arr);
    $str = "";
    foreach ($reducedArr as $key=>$value) {
      $str=$str." {".$key.":".$value."}";
    }
    return $str;
  }
  function reducedArray($arr) {
    $reducedArr = countDuplicate($arr);
    $str = array();
    foreach ($reducedArr as $key=>$value) {
      array_push($str,$key);
    }
    return implode(",", $str);
  }
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <?php
  require_once("./modules/jquery_boostrap_fontawesome.php");
  ?>
</head>

<body>
  <div class="container">
    <form action="">
      <div class="form-group">
        <label for="number">Nhập mảng</label>
        <input type="text" class="form-control" name="listnumber" placeholder="<?php echo $placeholder ?>">
      </div>
      <button type="submit" class="btn btn-primary">Thực hiện</button>
    </form>

    <div class="form-group" id="result">
      <label for="">Số lần xuất hiện</label>
      <input type="text" class="form-control" readonly value="<?php echo assocArrayToString($arr); ?>">
    </div>
    <div class="form-group" id="result">
      <label for="">Mảng rút gọn</label>
      <input type="text" class="form-control" readonly value="<?php echo reducedArray($arr); ?>">
    </div>
  </div>
  </div>
</body>
<style>
  #result {
    margin: 10px auto;
  }
</style>

</html>