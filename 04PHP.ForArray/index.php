<?php
$sum = 0;
$sumeven = 0;
$mul = 0;
$sumodd = 0;  

if (isset($_GET["fromNum"]) && isset($_GET["toNum"]) && is_numeric($_GET["fromNum"]) && is_numeric($_GET["toNum"])) {
  $mul = 1;
  for ($i = $_GET["fromNum"]; $i<=$_GET["toNum"];$i++) {
    $sum += $i;
    if ($i % 2 == 0) {
      $sumeven+=$i;
    }
    else {
      $sumodd+=$i;
    }
    $mul*=$i;
  }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>

<body>

  <!DOCTYPE html>
  <html lang="en">

  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <?php
    
    require_once("./modules/jquery_boostrap_fontawesome.php");
    ?>
  </head>

  <body>
    <div class="container">
      <form action="index.php">
        <div class="form-group">
          <label for="number">Từ số</label>
          <input type="text" class="form-control" name="fromNum">
        </div>
        <div class="form-group">
          <label for="number">Đến số</label>
          <input type="text" class="form-control" name="toNum">
        </div>
        <button type="submit" class="btn btn-primary">Tính toán</button>
      </form>

      <div id="result">
        <div class="form-group">
          <label for="number">Tổng các số</label>
          <input type="text" class="form-control" readonly value="<?php echo $sum ?>">
        </div>
        <div class="form-group">
          <label for="number">Tích các số</label>
          <input type="text" class="form-control" readonly value="<?php echo $mul ?>">
        </div>
        <div class="form-group">
          <label for="number">Tổng các số chẵn</label>
          <input type="text" class="form-control" readonly value="<?php echo $sumeven ?>">
        </div>
        <div class="form-group">
          <label for="number">Tổng các số lẻ</label>
          <input type="text" class="form-control" readonly value="<?php echo $sumodd ?>">
        </div>
      </div>
    </div>
    </div>
  </body>
  <style>
    #result {
      margin: 10px auto;
    }
  </style>

  </html>
</body>

</html>