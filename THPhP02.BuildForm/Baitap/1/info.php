<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<body>
  <div class="container">
    <div class="row">
      <p class="col-sm-2">Họ tên</p>
      <p class="col-sm-2"><?php echo $_POST["hoten"]?></p>
    </div>
    <div class="row">
      <p class="col-sm-2">Mã sinh viên</p>
      <p class="col-sm-2"><?php echo $_POST["masv"]?></p>
    </div>
    <div class="row">
      <p class="col-sm-2">Môn học</p>
      <p class="col-sm-2"><?php echo $_POST["monhoc"]?></p>
      
    </div>
    <div class="row">
      <p class="col-sm-2">Thang điểm 4</p>
      <p class="col-sm-2"><?php $diem=$_POST["diem"];
        if ($diem >= 8.5) {
          echo "A";
        }
        else if ($diem >=7) {
          echo "B";
        }
        else if ($diem >= 5.5) {
        echo "C";}
        else if ($diem >= 4) {
          echo "D";
        }
        else echo "F";

      ?></p>
    </div>
    <div class="row">
      <p class="col-sm-2">Kết quả</p>
      <p class="col-sm-2"><?php $diem=$_POST["diem"];
        if ($diem >= 9) {
          echo "Xuất sắc";
        }
        else if ($diem >=8) {
          echo "Giỏi";
        }
        else if ($diem >= 7) {
        echo "Khá";}
        else if ($diem >= 6) {
          echo "Trung bình khá";
        }
        else if ($diem >= 5) {
          echo "Trung bình";
        } else if ($diem >= 4) {
          echo "Yếu";
        } 
        else echo "Kém";

      ?></p>
    </div>
  </div>
</body>