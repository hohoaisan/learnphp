<?php
session_start();

if (!isset($_SESSION["toDoList"]))  {
  $_SESSION["toDoList"] = array("The quick  brown fox jumps", "over the lazy dog", "lorem ipsum dolor");
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Task List Manager</title>     
  <link rel="stylesheet" href="./style.css">
</head>
<body>
    <div class="container">
      <div class="box">
        <h1 class="box-title">Task List Manager</h1>
        <div class="box-body">
          <fieldset class="box-item">
            <legend><h2>Tasks</h2></legend>
            <ul class="task-list">
              <?php
              for ($i=0; $i<count($_SESSION["toDoList"]); $i++) {
                echo '<li class="task-item">'.$_SESSION["toDoList"][$i].'</li>';
              }
              ?>
              <!-- <li class="task-item">Plan the chapter</li>
              <li class="task-item">Write the chapter</li>
              <li class="task-item">Proofread the chapter</li> -->
            </ul>
          
          </fieldset>
          <fieldset class="box-item">
            <legend><h2>Add Task</h2></legend>
            <form class="form" action="add.php" method="POST">
              <input type="text" class="input" name="content">
              <button class="button">Add Task</button>
            </form>
          
          </fieldset>
          <fieldset class="box-item">
            <legend><h2>Delete Task</h2></legend>
            <form class="form" action="delete.php" method="POST">
              <select class="input" name="id" id="">
              <?php
              for ($i=0; $i<count($_SESSION["toDoList"]); $i++) {
                echo '<option value="'.$i.'">'.$_SESSION["toDoList"][$i].'</option>';
              }
              ?>
                <!-- <option value="1">Plan the chapter</option>
                <option value="2">Plan the chapter</option> -->
              </select>
              <button class="button">Delete Task</button>
            </form>
          
          </fieldset>
        </div>
      </div>
    </div>
</body>
</html>