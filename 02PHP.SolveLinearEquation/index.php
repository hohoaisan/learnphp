<?php

            $result = "";
            if (isset($_GET["a"]) && isset($_GET["b"])) {
                $a = $_GET["a"];
                $b = $_GET["b"];
                if ($a == 0) {
                    if ($b == 0)  {
                        $result = "Vô số nghiệm";
                    }
                    else {
                        $result = "Vô nghiệm";
                    }
                }
                else {
                    $result = -$b/$a;
                }
            }

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <?php require_once("./modules/jquery_boostrap_fontawesome.php") ?>

</head>

<body>
    <div class="container .container-fluid ">
        <h3>Giải phương trình bậc 1</h3>
        <form id="form" action="index.php">
            <div class="group row form-group">
                <input class="form-control col-sm-1" type="text" name="a" placeholder="a">
                <span class="form-control col-sm-1">x+</span>
                <input class="form-control col-sm-1" type="text" name="b" placeholder="b">
                <span class="form-control col-sm-1">=0</span>
                <button class="btn btn-primary" type="submit">Giải</button>
            </div>
        </form>
        <div class="result row">
            <label class=" col-sm-1 col-form-label" for="">Nghiệm</label>
            <input type="text" class="form-control col-sm-2" readonly value="<?php echo $result?>">
        </div>
 
    </div>


    </div>


</body>
<style>
    span.form-control {
        border: none;
    }
    .row {
        margin: auto;
    }
    .container > * {
        margin: 5px auto;
    }
</style>

</html>