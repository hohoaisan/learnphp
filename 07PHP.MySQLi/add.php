<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <?php
  require("./modules/jquery_boostrap_fontawesome.php");
  ?>
</head>

<body>
  <div class="container">
    <form action="./formSubmit.php" method="post" enctype="multipart/form-data">
      <div class="form-group">
        <label for="tentl">Tên thể loại</label>
        <input type="text" name="tentl" class="form-control">
      </div>
      <div class="form-group">
        <label for="tentl">Thứ tự</label>
        <input type="text" name="thutu" class="form-control">
      </div>
      <div class="form-group">
        <label for="anhien"> Ẩn hiện</label>
        <select name="anhien" id="anhien" class="custom-select">
          <option value="0">Ẩn</option>
          <option value="1">Hiện</option>
        </select>

      </div>
      <div class="form-group">
        <label for="icon">Icon</label>
        <input type="file" name="image" class="form-control">
      </div>

      <button class="btn btn-primary" type="submit">Gửi</button>
    </form>
  </div>
</body>

</html>