<?php
function solvePTBac2($a, $b, $c)
{
  $delta = $b * $b - 4 * $a * $c;
  if ($delta <= 0) {
    return "Phương trình vô nghiệm";
  } else if ($delta == 0) {
    return "Nghiệm kép " . (-$b / (2 * $a));
  } else return "x1=" . (-$b - sqrt($delta)) / (2 * $a) . ", x2=" . (-$b + sqrt($delta)) / (2 * $a);
}
$result = "";
if (isset($_GET["a"]) && isset($_GET["b"]) && isset($_GET["c"])) {
  $a = $_GET["a"];
  $b = $_GET["b"];
  $c = $_GET["c"];
  if (!!$a && !!$b && !!$c) {
    $result = solvePTBac2($a, $b, $c);
  }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>

<body>
  <div class="container">
    <form action="index.php" class="form">
      <div class="form-group">
        <input type="text" name="a">
        <span>x<sup>2</sup>+</span>
        <input type="text" name="b">
        <span>x+</span>
        <input type="text" name="c">
        <span>=0</span>
      </div>
      <div class="form-group">
        <button class="button" type="submit">Giải</button>
      </div>
    </form>
    <div class="result">
      <label for="">Kết quả</label>
      <input class="result-textbox" type="text" id="result" readonly value="<?php echo $result ?>">
    </div>
  </div>

</body>
<style>
  body {
    font-family: sans-serif;
  }

  .container {
    width: fit-content;
    margin: 0 auto;
  }
  .form-group {
    margin: 20px 0;
  }
  .result-textbox  {
    display: block;
    margin: 20px 0;
  }
</style>

</html>