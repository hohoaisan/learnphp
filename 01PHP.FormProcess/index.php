<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <?php require_once("./modules/jquery_boostrap_fontawesome.php") ?>

</head>

<body>
    <div class="container">
        <form action="index.php" method="get">
            <div class="form-group">
                <label for="name">Nhập họ tên của bạn</label>
                <input class="form-control" type="text" name="name">
            </div>
            <button class="btn btn-primary btn-block">Gửi</button>
        </form>
        <div class="result">
            <?php
            if (isset($_GET["name"]) && $_GET["name"] != "") {
                echo '<div class="alert alert-success" role="alert"> Xin chào ' . $_GET["name"] . '</div>';
            }
            ?>
        </div>


    </div>


</body>
<style>
    .result {
        margin: 20px 0;

    }
</style>

</html>