<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <link rel="stylesheet" href="./global.css">
</head>

<body>
  <div class="container">
    <div class="product">
      <div class="detail">
        <div class="detail-group">
          <label>Tên sản phẩm</label>
          <input type="text" readonly value="Apple iphoddddDdDddddddddddddddddddddddddddddddddddddddđne 5">
        </div>
        <div class="detail-group">
          <label>Giá bán</label>
          <input type="text" readonly value="1999999đ">
        </div>
        <div class="detail-group">
          <label>Xuất xứ</label>
          <input type="text" readonly value="Việt Nam">
        </div>

        <div class="detail-group">
          <label>Hệ điều hành</label>
          <input type="text" readonly value="IOS 54">
        </div>
        <div class="detail-group">
          <label>Nhà phân phối</label>
          <input type="text" readonly value="Thế giới di động">
        </div>

        
      </div>
      <div class="image">
        <img src="https://via.placeholder.com/400x500" alt="Sản phẩm có một không hai">
      </div>
    </div>
  </div>
</body>
<style>
  .product {
    display: grid;
    grid-template-columns: 1fr 1fr;
    align-content: stretch;
    margin: 10px 0;
    box-shadow: 0 0 100px 1px lightgray;
    border-radius: 5px;
    box-sizing: border-box;
    padding: 10px;
    align-self: center;
    height: 550px;
  }

  .product .detail {

    box-sizing: border-box;
    padding: 10px;
  }
  .product .image {
    display: flex;
    justify-items: center;
    align-items: center;
    flex-direction: row;
  }
  .product .image img {
    display: block;
    margin: 0 auto;
  }
  .detail-group {
    box-sizing: border-box;
    margin-bottom: 20px;
  }

  .detail-group label {
    font-size: 1.1em;
    display: block;
    font-weight: 300;
    margin-bottom: 5px;
  }

  .detail-group input {
    display: block;
    padding: 5px;
    font-size: 1.1em;
    box-sizing: border-box;
    width: 100%;

  }
</style>

</html>