<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <link rel="stylesheet" href="./global.css">
</head>

<body>
  <div class="container">
    <h1>Chọn ngày tháng</h1>
    <form action="">
      <div class="select-group">
        <select name="date" id="">
          <?php
          for ($i = 1; $i <= 31; $i++) {
            echo "<option value=" . $i . ">" . $i . "</option>";
          }
          ?>
        </select>
        <select name="month" id="">
          <?php
          for ($i = 1; $i <= 12; $i++) {
            echo "<option value=" . $i . ">" . $i . "</option>";
          }
          ?>
        </select>
        <select name="year" id="">
          <?php
          for ($i = 1970; $i <= 2020; $i++) {
            echo "<option value=" . $i . ">" . $i . "</option>";
          }
          ?>
        </select>
      </div>
      <button id="submit" type="submit" class="btn btn-primary">Submit</button>
    </form>

  </div>
</body>

<style>
  .blue-line {
    background-color: lightskyblue;
  }
</style>

<script>
  submit.onclick = function(event) {
    event.preventDefault();
    f = document.querySelector('form');
    fd = new FormData(f);
    console.log(...fd);
  }
</script>

</html>