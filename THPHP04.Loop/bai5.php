<?php
session_start();
$json = '
[
    {
      "id": 1,
      "name": "Iphone 4",
      "price": "23000",
      "origin": "Apple",
      "os": "iOS",
      "distributor": "Thế giới di động",
      "image": "https://via.placeholder.com/200x200"
    },
    {
      "id": 2,
      "name": "Redmi 6",
      "price": "1600000",
      "origin": "Xiaomi",
      "os": "MIUI",
      "distributor": "Heaven",
      "image": "https://via.placeholder.com/200x200"
    },
    {
      "id": 3,
      "name": "Windows Phone",
      "price": "1600000",
      "origin": "Microsoft",
      "os": "Windows 10",
      "distributor": "Hell",
      "image": "https://via.placeholder.com/200x200"
    }
]
';
$_SESSION["data"] = json_decode($json, true);
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <link rel="stylesheet" href="./global.css">
</head>

<body>
  <div class="container">

    <table>
      <caption>Danh sách sản phẩm</caption>
      <thead>
        <tr>
          <td>STT</td>
          <td>Nội dung</td>
          <td>Hình ảnh</td>
          <td>Thao tác</td>
        </tr>
      </thead>
      <tbody>
        <?php

        for ($i = 0; $i < count($_SESSION["data"]); $i++) {
          echo "
      <tr>
        <td>
        " . ($i + 1)
            . "
        </td>
        <td>" .
        $_SESSION["data"][$i]["name"]
            . "</td>
        <td><img class='img-preview' src='" .
        $_SESSION["data"][$i]["image"]."'></td><td><a href='bai5_show.php?index=".$i."'>Xem chi tiết</a></td>
      </tr>
    ";
        }
        ?>
      </tbody>
    </table>

  </div>
</body>

<style>
  td {
    border: 1px solid black;
    padding: 5px;
  }

  table {
    border-collapse: collapse;
  }

  .img-preview {
    width: 50px;
  }

  thead {
    background: lightgreen;
  }
</style>

</html>